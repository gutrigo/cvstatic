# Detalhes do Projeto
## Projeto de Curriculo Online

1. O projeto consiste em demonstrar minhas habilidades em Javascript, Jquery, css e HTML5 de forma interativa.
2. Armazeno minhas informações profissionais em objetos javascripts.
3. Utilizado o javascript para ler os objetos e construir um CV dinamico.

### Por Fim:
O projeto contém os seguintes arquivos.

* **index.html**: O documento HTML principal. Contém links para todos os recursos CSS e JS necessários para renderizar o currículo, incluindo resumeBuilder.js.
* **js/helper.js**: Contém o código auxiliar necessário para formatar o currículo e criar o mapa. Ele também possui alguns shells de função para funcionalidades adicionais.
* **js/resumeBuilder.js**: O resumeBuilder é responsável pela manipulação do dado e construção da página HTML.
* **js/jQuery.js**: Biblioteca Jquery.
* **css/style.css**: Contém todos os CSS necessários para modelar a página.
* **README.md**: 
* e algumas imagens no diretório de imagens.

