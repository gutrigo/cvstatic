/*
    JavaScript Objects
 */

var bio = {
    "info": [{
        "name": "Fulano de Tal",
        "role": "Desenvolvedor Web",
        "welcomeMessage": "Olá Mundo !!!",
        "skills": ["HTML5", "CSS3", "JavaScript", "GruntJs", "Bootstrap", "Jquery", "Java", "AngularJs", "Angular"],
        "contacts": [{
            "email": "fulano@beltrano.com",
            "cell": "+55 11 9999-9999",
            "githubUser": "user",
            "location": "Mundo-sp"
        }],
        "picture": "images/fry.jpg",
    }],
    "display": function() {
        //*         Insert Bio, header and footer
        var formattedName = HTMLheaderName.replace("%data%", bio.info[0].name);
        var formattedRole = HTMLheaderRole.replace("%data%", bio.info[0].role);
        var formattedEmail = HTMLemail.replace("%data%", bio.info[0].contacts[0].email);
        var formattedCell = HTMLmobile.replace("%data%", bio.info[0].contacts[0].cell);
        var formattedGitHub = HTMLgithub.replace("%data%", bio.info[0].contacts[0].githubUser);
        var formattedLocation = HTMLlocation.replace("%data%", bio.info[0].contacts[0].location);
        var formattedPicture = HTMLbioPic.replace("%data%", bio.info[0].picture);
        var formattedWelcome = HTMLwelcomeMsg.replace("%data%", bio.info[0].welcomeMessage);

        $("#header").prepend(formattedRole);
        $("#header").prepend(formattedName);

        $("#topContacts").append(formattedEmail);
        $("#topContacts").append(formattedCell);
        $("#topContacts").append(formattedGitHub);
        $("#topContacts").append(formattedLocation);
        $("#header").append(formattedPicture);
        $("#header").append(formattedWelcome);
        $("#header").append(HTMLskillsStart);


        if (bio.info[0].skills.length > 0) {

            var formattedSkills;

            for (var skill in bio.info[0].skills) {
                formattedSkills = HTMLskills.replace("%data%", bio.info[0].skills[skill]);
                $("#skills").append(formattedSkills);
            }
        }

        $("#footerContacts").append(formattedEmail);
        $("#footerContacts").append(formattedCell);
        $("#footerContacts").append(formattedGitHub);
        $("#footerContacts").append(formattedLocation);

        }
}

/*
Aux variables
*/

var HTMLupDown = '<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>';
var HTMLheaderName = '<h1 id="name">%data%</h1></br>';
var HTMLheaderRole = '<span>%data%</span><hr>';

var HTMLmobile = '<span class="flex-item"><span class="orange-text">mobile</span><span class="white-text">%data%</span></span>';
var HTMLemail = '<span class="flex-item"><span class="orange-text">email</span><span class="white-text">%data%</span></span>';
var HTMLgithub = '<span class="flex-item"><span class="orange-text">github</span><span class="white-text">%data%</span></span>';
var HTMLlocation = '<span class="flex-item"><span class="orange-text">location</span><span class="white-text">%data%</span></span>';


var HTMLbioPic = '<picture><img src="%data%" class="biopic" alt="Foto de perfil"></picture>';
var HTMLwelcomeMsg = '<span class="welcome-message white-text ">%data%</span>';

var HTMLskillsStart = '<h3 id="skills-h3">Skills:</h3><ul id="skills" class="flex-column"></ul>';
var HTMLskills = '<li class="flex-item"><span class="white-text">%data%</span></li>';


/* Execute display function to build the page*/

//*         Bio
bio.display();

